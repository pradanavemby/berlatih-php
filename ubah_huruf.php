<?php
    function ubah_huruf($string){
    //kode di sini
        $output = "";
       /*  $abjad = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]; */
        $abjad = "abcdefghijklmnopqrstuvwxyz";

        for ($i = 0; $i < strlen($string); $i++){
            $posisi = strrpos($abjad, $string[$i]);
            $output .= substr($abjad, $posisi + 1, 1);
        }
        return "$string diubah jadi $output <br>";

        /* for ($i = 0; $i < strlen($string); $i++){
            for ($j = 0 ; $j < count($abjad); $j++){
                if ($string[$i] == $abjad[$j]){
                    $ubahAbjad .= $abjad[$j + 1];
                }   
            } 
        } */
    }
    
    // TEST CASES
    echo ubah_huruf('wow'); // xpx
    echo ubah_huruf('developer'); // efwfmpqfs
    echo ubah_huruf('laravel'); // mbsbwfm
    echo ubah_huruf('keren'); // lfsfo
    echo ubah_huruf('semangat'); // tfnbohbu

?>